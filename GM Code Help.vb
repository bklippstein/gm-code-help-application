Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Text
Imports System.String
Imports System.Windows.Forms.TextBoxBase
Imports System.Text.RegularExpressions


Public Class frmMain
    Public Shared Lflinetext As String
    Public Shared GFound As Boolean
    Public Shared MFound As Boolean
    Public Shared numflag As Integer
    Public Shared linetextfromG As String
    Public Shared linetextfromGM As String
    Public shared activeG As String

    Public Shared t As Regex()
    Public line As String



    Private Sub keyup_event(textbox1 As System.Object, e As KeyEventArgs) Handles TextBox1.KeyUp

        If e.KeyCode = Keys.Enter Then

        ElseIf e.KeyCode = Keys.Down Then

            'If Not Me.ComboBox1.DroppedDown Then
            '    Me.ComboBox1.DroppedDown = True

            Me.ComboBox1.SelectedIndex += 1
            Exit Sub
            'End If
        ElseIf e.KeyCode = Keys.Up Then

            ' ElseIf e.KeyCode = Keys.Down Then
            'If Not Me.ComboBox1.DroppedDown Then
            '    Me.ComboBox1.DroppedDown = True
            'End If
            'Me.ComboBox1.SelectedIndex -= 1
            Exit Sub
        End If

        Dim cursorIndex As Integer = Me.TextBox1.SelectionStart
        Dim lineIndex As Integer = Me.TextBox1.GetLineFromCharIndex(cursorIndex)
        Dim startLineTextBack As String
        Dim Gcode1 As String '1st digit of G code
        Dim Gcode2 As String ' 2nd digit of G code
        Dim Gcode3 As String '  3rd digit of G code
        Dim Mcode1 As String '1st digit of M code
        Dim Mcode2 As String '  2nd digit of M code
        Dim Mcode3 As String '   3rd digit of M code
        Dim First2 As String
        Dim First3 As String
        Dim First4 As String
        Dim first5 As String 
        Dim get_digit_after As String
        Dim boxItemAdd As String
        Dim dupe As Boolean = False

        ComboBox1.Items.Clear()
        ComboBox1.Visible = False

        Look4G_M()

        Dim lineText = Me.TextBox1.Lines(lineIndex)
        Dim charcount As Integer = Me.TextBox1.Text.Length



        Lflinetext = lineText.Substring(0, charcount)

        

        ' linetextfromG = Lflinetext.Trim(Lflinetext)
        linetextfromGM = LTrim(Lflinetext)
        Dim LflineTextBack As String

        Look4Number()

        If numflag = Nothing Then

            Exit Sub

        End If

        Dim folder As String
        Dim filename As String

        If Lflinetext.Contains("G") Then
            folder = "C:\Users\test\Documents"
            filename = System.IO.Path.Combine(folder, Name & ".JPEG")

            If linetextfromGM.Count < 2 Then
                MsgBox("Error")
                Exit Sub
            End If

            If linetextfromGM.Length = 2 Then
                First2 = linetextfromGM.Substring(0, 2) ' Get First G or M code & digit after
                get_digit_after = linetextfromGM.Substring(1, 1)
            End If

            If linetextfromGM.Length = 3 Then
                First3 = linetextfromGM.Substring(0, 3) ' Get First G or M code and next 2 digits
                get_digit_after = linetextfromGM.Substring(1, 2)
            End If

            If linetextfromGM.Length > 3 Then
                First4 = linetextfromGM.Substring(0, 4)
                get_digit_after = linetextfromGM.Substring(1, 3)
            End If

            Me.TextBox3.Text = IO.File.ReadAllText("C:\Users\test\Documents\Lathe_G_List.txt")
            Me.TextBox3.Text = Me.TextBox3.Text.Replace(":", "")
            Me.TextBox3.Text = Me.TextBox3.Text.Replace("#", "")
            Me.TextBox3.Text = Me.TextBox3.Text.Replace("""", " ")
            Me.TextBox3.Text = Me.TextBox3.Text.Replace(",", "  ")
            Dim LineCount As Integer = TextBox3.Lines.Count




            For i As Integer = 0 To LineCount
                For Each line As String In Me.TextBox3.Lines
                    If line.StartsWith(" G") Then 'Or line.StartsWith(" M") Then
                        Me.TextBox4.AppendText(line & Environment.NewLine)
                    End If
                    i = i + 1
                Next
            Next

            Dim lines() As String = System.Text.RegularExpressions.Regex.Split(Me.TextBox4.Text, Environment.NewLine)


            Dim newTextLines As New List(Of String)

            For Each line As String In lines
                If line.Contains(linetextfromGM) Then

                    If ComboBox1.Items.Contains(line) Then
                        dupe = True
                    End If

                    If dupe = False Then
                        ComboBox1.Items.Add(line)
                    End If

                    If Not line.Contains(linetextfromGM) Then
                        Me.ComboBox1.DroppedDown = False
                        ComboBox1.SelectedIndex = -1
                        ComboBox1.Visible = False
                        Exit Sub


                    End If


                End If

                If line.Equals(linetextfromGM) Then

                    Me.TextBox2.Text = line.ToString
                    PictureBox1.Image = Image.FromFile("C:\Users\test\Documents\G00.jpg")
                    Exit Sub
                End If

                If Not line.Contains(linetextfromGM) Then
                    ComboBox1.Visible = False
                    PictureBox1.Image = Nothing

                End If


            Next


        End If


        If Lflinetext.Contains("M") Then
            folder = "C:\Users\test\Documents"
            filename = System.IO.Path.Combine(folder, Name & ".JPEG")

            If linetextfromGM.Count < 2 Then
                MsgBox("Error")
                Exit Sub
            End If

            If linetextfromGM.Length = 2 Then
                First2 = linetextfromGM.Substring(0, 2) ' Get First M code & digit after
                get_digit_after = linetextfromGM.Substring(1, 1)
            End If

            If linetextfromGM.Length = 3 Then
                First3 = linetextfromGM.Substring(0, 3) ' Get First M code and next 2 digits
                get_digit_after = linetextfromGM.Substring(1, 2)
            End If

            If linetextfromGM.Length =4 Then
                First4 = linetextfromGM.Substring(0, 4)
                get_digit_after = linetextfromGM.Substring(1,3)
            End If

            If linetextfromGM.Length=5 Then
                First5 = linetextfromGM.Substring(0, 5)
                get_digit_after = linetextfromGM.Substring(1, 4)
                End If


            Me.TextBox3.Text = IO.File.ReadAllText("C:\Users\test\Documents\Lathe_M_List.txt")
            Me.TextBox3.Text = Me.TextBox3.Text.Replace(":", "")
            Me.TextBox3.Text = Me.TextBox3.Text.Replace("#", "")
            Me.TextBox3.Text = Me.TextBox3.Text.Replace("""", " ")
            Me.TextBox3.Text = Me.TextBox3.Text.Replace(",", "  ")
            Dim MLineCount As Integer = TextBox3.Lines.Count

            For i As Integer = 0 To MLineCount
                For Each line As String In Me.TextBox3.Lines
                    If line.StartsWith(" G") Or line.StartsWith(" M") Then
                        Me.TextBox4.AppendText(line & Environment.NewLine)
                    End If
                    i = i + 1
                Next
            Next

            Dim MLines() As String = System.Text.RegularExpressions.Regex.Split(Me.TextBox4.Text, Environment.NewLine)


            Dim newTextLinesM As New List(Of String)

            For Each Mline As String In MLines
                If Mline.Contains(linetextfromGM) Then

                    If ComboBox1.Items.Contains(Mline) Then
                        dupe = True
                    End If

                    If dupe = False Then
                        ComboBox1.Items.Add(Mline)
                    End If

                    If Not Mline.Contains(linetextfromGM) Then
                        Me.ComboBox1.DroppedDown = False
                        ComboBox1.SelectedIndex = -1
                        ComboBox1.Visible = False
                        Exit Sub


                    End If

                End If
            Next
        End If





        If ComboBox1.Items.Count > 0 Then
            Me.ComboBox1.Visible = True
            Me.ComboBox1.DroppedDown = True
            Me.ComboBox1.SelectedIndex = 0
            Me.ComboBox1.Select(0, 2)
        End If
        If ComboBox1.Items.Count = 0 Then
            Me.ComboBox1.DroppedDown = False
            ComboBox1.Visible = False
            Me.ComboBox1.SelectedIndex = -1
        End If

    End Sub
    Public Sub Look4Number()
        Dim objRegExp As New System.Text.RegularExpressions.Regex("\s*^[GM]([0-9]{1,3})")
        Dim match As System.Text.RegularExpressions.Match = objRegExp.Match(linetextfromGM)
        If match.Success Then
            'Return True
            numflag = 1
            Exit Sub

        End If

        numflag = 0


    End Sub
    Public Sub Look4G_M()
        Do Until TextBox1.Text.Contains("G") Or TextBox1.Text.Contains("M")
            Threading.Thread.Sleep(50)
            Application.DoEvents()
        Loop
        If TextBox1.Text.Contains("G") Then
            GFound = 1
            MFound = 0
            Exit Sub
        End If


        If TextBox1.Text.Contains("M") Then
            GFound = 0
            MFound = 1
        End If


    End Sub

    Private Sub combox_selectedIndexChanged(sender1 As Object, ByVal e As KeyEventArgs) Handles ComboBox1.KeyDown

        'If n.KeyChar = ChrW(Keys.Return) Then

        'If e.KeyCode = Keys.Enter Then
        '    Dim testChar = Me.TextBox1.Text.ToCharArray
        '    For i As Integer = testChar.Length - 1 To 0 Step -1
        '        If Not (testChar(i).ToString = "G") Or (testChar(i).ToString = "M") Then
        '            Me.TextBox1.Text = Me.TextBox1.Text.Remove(Me.TextBox1.Text.Length - 1, 1)
        '        Else
        '            Me.TextBox1.Text = Me.TextBox1.Text.Remove(Me.TextBox1.Text.Length - 1, 1)
        '            Exit For
        '        End If
        '    Next

        '    TextBox1.AppendText(ComboBox1.SelectedText.ToString)
        'End If
        'End If
    End Sub


End Class



